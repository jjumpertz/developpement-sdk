<?php
const CLIENT_ID = "client_60a3778e70ef02.05413444";
const CLIENT_FBID = "3648086378647793";
const CLIENT_SECRET = "cd989e9a4b572963e23fe39dc14c22bbceda0e60";
const CLIENT_FBSECRET = "1b5d764e7a527c2b816259f575a59942";
const STATE = "fdzefzefze";
const GOOGLE_ID = "346954581289-db2qgdg68ejc8nkunvoedrjhu05k9si0.apps.googleusercontent.com";
const GOOGLE_SECRET = "JjaEpa_t62KcL-kdEoLUXHbF";
const TWITCH_ID = "e09bmsdkyvl8jevg0j2v4oyhw2d6nl";
const TWITCH_SECRET = "xm75x7ijvwx51hjh7ir6e29o7p6eq7";



function handleLogin()
{
    // http://.../auth?response_type=code&client_id=...&scope=...&state=...
    echo "<h1>Login with OAUTH</h1>";
    echo "<a href='http://localhost:8081/auth?response_type=code"
        . "&client_id=" . CLIENT_ID
        . "&scope=basic"
        . "&state=" . STATE . "'>Se connecter avec Oauth Server</a></br>";
    echo "<a href='https://www.facebook.com/v2.10/dialog/oauth?response_type=code"
        . "&client_id=" . CLIENT_FBID
        . "&scope=email"
        . "&state=" . STATE
        . "&redirect_uri=https://localhost/fbauth-success'>Se connecter avec Facebook</a></br>";
    echo "<a href='https://accounts.google.com/o/oauth2/v2/auth?"
        . "scope=https://www.googleapis.com/auth/userinfo.profile"
        . "&state=" . STATE
        . "&access_type=offline"
        . "&redirect_uri=https://localhost/ggleauth-success"
        . "&response_type=code"
        . "&client_id=" . GOOGLE_ID ."'>Se connecter avec google</a></br>";
    
    echo "<a href='https://id.twitch.tv/oauth2/authorize"
        . "?client_id=" . TWITCH_ID
        . "&redirect_uri=https://localhost/twitchOAuth-success"
        . "&response_type=code"
        . "&scope=viewing_activity_read"
        . "&state=".STATE."'> Se connecter avec twitch</a></br>";

        if(isset($_SESSION['response'])){
            echo $_SESSION['response'];
        }
}

function handleError()
{
    ["state" => $state] = $_GET;
    echo "{$state} : Request cancelled";
}

function handleSuccess()
{
    ["state" => $state, "code" => $code] = $_GET;
    if ($state !== STATE) {
        throw new RuntimeException("{$state} : invalid state");
    }
    // https://auth-server/token?grant_type=authorization_code&code=...&client_id=..&client_secret=...
    getUser([
        'grant_type' => "authorization_code",
        "code" => $code,
    ]);
}

function handleFbSuccess()
{
    ["state" => $state, "code" => $code] = $_GET;
    if ($state !== STATE) {
        throw new RuntimeException("{$state} : invalid state");
    }
    $url = "https://graph.facebook.com/oauth/access_token?grant_type=authorization_code&code={$code}&client_id=" . CLIENT_FBID . "&client_secret=" . CLIENT_FBSECRET."&redirect_uri=https://localhost/fbauth-success";
    $result = file_get_contents($url);
    $resultDecoded = json_decode($result, true);
    ["access_token"=> $token] = $resultDecoded;
    $userUrl = "https://graph.facebook.com/me?fields=id,name,email";
    $context = stream_context_create([
        'http' => [
            'header' => 'Authorization: Bearer ' . $token
        ]
    ]);
    echo file_get_contents($userUrl, false, $context);
}

function getUser($params)
{
    $url = "http://oauth-server:8081/token?client_id=" . CLIENT_ID . "&client_secret=" . CLIENT_SECRET . "&" . http_build_query($params);
    $result = file_get_contents($url);
    $result = json_decode($result, true);
    $token = $result['access_token'];

    $apiUrl = "http://oauth-server:8081/me";
    $context = stream_context_create([
        'http' => [
            'header' => 'Authorization: Bearer ' . $token
        ]
    ]);
    echo file_get_contents($apiUrl, false, $context);
}

function handleGgleSuccess(){
    $uri = http_build_query([
        'redirect_uri' => 'https://localhost/ggleauth-success',
        'client_id' => GOOGLE_ID,
        'client_secret' => GOOGLE_SECRET,
        'code' => $_GET['code'],
        'grant_type' => 'authorization_code'
    ]);
    $url = "https://oauth2.googleapis.com/token?" . $uri;
    $context = stream_context_create([
        'http' => [
            'method' => 'POST',
            'user_agent' => 'Google Oauth2.0',
            'header' => 'Accept: application/json\r\n' . 'Content-type: application/x-www-form-urlencoded\r\n' . 'Content-Length: '.strlen($uri) . '\r\n',
            'content' => $uri
        ]
    ]);
    $result = file_get_contents($url, false, $context);
    $result = json_decode($result);
    $token = $result['access_token'];
    $urlprofile = "https://www.googleapis.com/auth/userinfo.profile";
    $context = stream_context_create([
        'http' => [
            'method' => 'GET',
            'header' => 'Authorization: Bearer ' . $token
        ]
    ]);
    $response = file_get_contents($urlprofile, false, $context);
    $response = json_decode($response);
    $_SESSION['response'] = $response->name;
    header('Location: '.handleLogin());
}


function handleTwitchSuccess(){
    $uri = http_build_query([
        'redirect_uri' => 'https://localhost/twitchOAuth-success',
        'client_id' => TWITCH_ID,
        'client_secret' => TWITCH_SECRET,
        'code' => $_GET['code'],
        'grant_type' => 'authorization_code'
    ]);
    $url = "https://id.twitch.tv/oauth2/token?" . $uri;
    $context = stream_context_create([
        'http' => [
            'method' => 'POST',
            'user_agent' => 'Google Oauth2.0',
            'header' => 'Accept: application/json\r\n' . 'Content-type: application/x-www-form-urlencoded\r\n' . 'Content-Length: '.strlen($uri) . '\r\n',
            'content' => $uri
        ]
    ]);
    $result = file_get_contents($url, false, $context);
    $result = json_decode($result);
    $token = $result['access_token'];
    $urlprofile = "https://id.twitch.tv/oauth2/validate";
    $context = stream_context_create([
        'http' => [
            'method' => 'GET',
            'header' => 'Authorization: Bearer ' . $token
        ]
    ]);
    $response = file_get_contents($urlprofile, false, $context);
    $response = json_decode($response);
    $_SESSION['response'] = $response->login;
    header('Location: '.handleLogin());
    echo file_get_contents($urlprofile, false, $context);
}
/**
 * AUTH CODE WORKFLOW
 * => Generate link (/login)
 * => Get Code (/auth-success)
 * => Exchange Code <> Token (/auth-success)
 * => Exchange Token <> User info (/auth-success)
 */
$route = strtok($_SERVER["REQUEST_URI"], "?");
switch ($route) {
    case '/login':
        handleLogin();
        break;
    case '/auth-success':
        handleSuccess();
        break;
    case '/fbauth-success':
        handleFbSuccess();
        break;
    case '/ggleauth-success':
        handleGgleSuccess();
        break;
    case '/twitchOAuth-success':
        handleTwitchSuccess();
        break;
    case '/auth-cancel':
        handleError();
        break;
    case '/password':
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            echo '<form method="POST">';
            echo '<input name="username">';
            echo '<input name="password">';
            echo '<input type="submit" value="Submit">';
            echo '</form>';
        } else {
            ["username" => $username, "password" => $password] = $_POST;
            getUser([
                'grant_type' => "password",
                "username" => $username,
                "password" => $password
            ]);
        }
        break;

    default:
        http_response_code(404);
        break;
}
